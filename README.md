## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


Purpose: Building a shopping cart with following functionalities: 

API used: `https://next.json-generator.com/api/json/get/4kiDK7gxZ`

=> I have created a grid for product listings with the image, price and title information using ngFor. 

=> when we click on an item the container expands and displays additional information, i.e. description, price, remaining stock & add to cart button

=> I have also added a quick view that displays interstitial view for that product. There is not much information in the API so I haven't created any page for each product

=> I don't think the given API provides support for post/patch or put methods so I have used session variables to keep track of stock for that session.

=> when a user clicks on "Add to Cart button", the stock session variable for that product id will be decremented and updated in the detailed view. Also I have a event emitter that emits event to product page where I add the product to the cart_items array.

=> If a user removes an item from that cart, then the session variable for that product will be replaced with original number and the item is removed from the cart.

=> when the product stock number equals zero, user will no longer be able to add items to the cart.

=> I have added a checkout button with a logic to avoid user checking out when there are no items in the cart.

=> when the "CHECKOUT" button is clicked I have a logic to clear the cart and display success message. 

=> I have added a small view to display length of the cart near the cart icon.

=> I have used fontawesome CDN for icons

Files:

Services/products-page.service.ts :  Http client requests to fetch data from given API

pages/products-page

components/product-lists

styles.less: global styes
