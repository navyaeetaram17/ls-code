import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ProductPageService} from '../../services/products-page.service';

interface ProductDetails {
  description: string;
  color: string;
  image: string;
  price: string;
  stock: any;
  title: string;
  _id: string;
}
@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.less']
})
export class ProductsPageComponent implements OnInit {
  product_listings: Array<ProductDetails>;
  cart_items: Array<ProductDetails> = [];
  cart_length = 0;
  cart_total = 0;
  checkout_button_text = 'checkout';
  @ViewChild('cartFold', {read: ElementRef}) cartFold: ElementRef;

  constructor(private _pps: ProductPageService, private _render: Renderer2) { }

  ngOnInit() {
    this.getProductsDetails();
  }

  /**
   * Purpose: Get product lists from given API
   */
  getProductsDetails() {
    this._pps.getProductDetails().subscribe(data => {
      this.product_listings = <Array<ProductDetails>>data;
      this.product_listings.forEach(each_listing => {
        if (sessionStorage.getItem(each_listing._id) === null) {
          sessionStorage.setItem(each_listing._id, each_listing.stock.remaining);
        }
      });
    });
  }

  /**
   * Purpose: when a product is added to cart, adds the product details to cart_items array
   * @param data : product details
   * dependency: add_to_cart event emitter
   */
  addProductToCart(data) {
    this.cart_items.push(data);
    this.cart_length = this.cart_items.length;
    let total_price = 0;
    this.cart_items.forEach(each_cart => {
      total_price = total_price + Number(each_cart.price.substring(1).replace(',', ''));
    });
    this.cart_total = total_price;
    this._render.setStyle(this.cartFold.nativeElement, 'display', 'flex');
  }

  /**
   * purpose: Remove a product from the cart
   * @param e: event
   * @param data: product details item
   * @param index: index
   */
  removeProduct(e, data, index) {
    let current_count = Number(sessionStorage.getItem(data._id));
    current_count = current_count + 1;
    sessionStorage.setItem(data._id, current_count.toString());

    this.cart_items.splice(index, 1);
    this.cart_length = this.cart_items.length;
    let total_price = 0;
    this.cart_items.forEach(each_cart => {
      total_price = total_price + Number(each_cart.price.substring(1).replace(',', ''));
    });
    this.cart_total = total_price;
  }

  /**
   * purpose: User clicks checkout, shows a success message and clears the cart
   * @param e : event
   */
  checkout(e) {
    if (this.cart_items.length > 0) {
      this.cart_items = [];
      this.cart_length = this.cart_items.length;
      this._render.setStyle(this.cartFold.nativeElement, 'height', '300px');
      this._render.setStyle(this.cartFold.nativeElement, 'display', 'flex');

      this._render.setStyle(this.cartFold.nativeElement, 'justify-content', 'center');
      const success_title = this._render.createElement('div');
      success_title.className = 'product-page-success';
      success_title.innerHTML = 'Order Successful';

      const success_description = this._render.createElement('div');
      success_description.className = 'product-page-success';
      success_description.innerHTML = 'Thank you for shopping with us. Your Order is in progress !! We’ll send a confirmation when your item ships :)';


      this.cartFold.nativeElement.innerHTML = '';
      this.cartFold.nativeElement.appendChild(success_title);
      this.cartFold.nativeElement.appendChild(success_description);

      setTimeout(() => {
        this._render.setStyle(this.cartFold.nativeElement, 'display', 'none');
        location.reload();
      }, 3000);
    } else {
      this.checkout_button_text = 'Empty Cart';
      this._render.setStyle(
        e.target,
        'animation',
        'shake 0.82s cubic-bezier(.36,.07,.19,.97) both'
      );
      this._render.setStyle(
        e.target,
        'transform',
        'translate3d(0, 0, 0)'
      );
      this._render.setStyle(
        e.target,
        'backface-visibility',
        'hidden'
      );
      this._render.setStyle(e.target, 'perspective', '1000px');
      this._render.setStyle(e.target, 'background', 'red');
      setTimeout(() => {
        this._render.setStyle(e.target, 'animation', 'none');
        this._render.setStyle(e.target, 'transform', 'none');
        this._render.setStyle(
          e.target,
          'backface-visibility',
          'none'
        );
        this.checkout_button_text = 'checkout';
        this._render.setStyle(e.target, 'perspective', 'none');
        this._render.setStyle(
          e.target,
          'background',
          '#abbaab'
        );
      }, 820);
    }

  }

  /**
   * purpose: toggle cart display
   * @param e: event
   */
  openCart(e) {
    if (this.cartFold.nativeElement.style.display == 'none') {
      this._render.setStyle(this.cartFold.nativeElement, 'display', 'flex');
      this._render.addClass(this.cartFold.nativeElement, 'active');
      this._render.removeClass(this.cartFold.nativeElement, 'out');

    } else {
      this._render.setStyle(this.cartFold.nativeElement, 'display', 'none');
      this._render.addClass(this.cartFold.nativeElement, 'out');
      this._render.removeClass(this.cartFold.nativeElement, 'active');

    }
  }

}
