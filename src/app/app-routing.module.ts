import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductsPageComponent} from './pages/products-page/products-page.component';

const routes: Routes = [
  {
    path: 'product-listings',
    component: ProductsPageComponent
  },
  {
    path: '',
    redirectTo: 'product-listings',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
