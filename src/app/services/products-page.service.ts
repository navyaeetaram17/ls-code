import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ProductPageService {
  constructor(private _http: HttpClient) {}

  getProductDetails() {
    const product_url = 'https://next.json-generator.com/api/json/get/4kiDK7gxZ';
    return this._http.get(product_url);
  }
}
