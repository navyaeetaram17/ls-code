import {
  Component,
  OnInit,
  Input,
  Renderer2,
  Output,
  EventEmitter,
  ViewEncapsulation,
  ElementRef,
  ViewChild
} from '@angular/core';
interface ProductDetails {
  description: string;
  color: string;
  image: string;
  price: string;
  stock: any;
  title: string;
  _id: string;
}
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ProductListComponent implements OnInit {
  @Input() product_listings: Array<ProductDetails>;
  @Output() add_to_cart: EventEmitter<ProductDetails> = new EventEmitter<ProductDetails>();
  @ViewChild('quickView', {read: ElementRef}) quickView: ElementRef;
  @ViewChild('closeInterstitial', {read: ElementRef}) closeInterstitial: ElementRef;


  interstitial_data: ProductDetails;

  constructor( private _render: Renderer2, private _eref: ElementRef) { }

  ngOnInit() {
  }

  /**
   * purpose: Expand the card view to display more details about the product
   * @param e: event
   * @param data: product details item
   */
  expandCard(e, data) {
    const button_wrapper = e.target.getElementsByClassName('pp-overview-card-wrap')[0];
    const image_wrapper = e.target.getElementsByClassName('pp-overview-card-img')[0];
    const check_details_element = e.target.getElementsByClassName('pp-overview-card-description-wrap');
    if (check_details_element.length === 0) {
      const desciption_wrapper = this._render.createElement('div');
      desciption_wrapper.className = 'pp-overview-card-description-wrap';

      const stock_row = this._render.createElement('div');
      stock_row.className = 'pp-overview-card-description-wrap-row';

      const color_element = this._render.createElement('div');
      color_element.className = 'pp-overview-card-description-wrap-row-color';

      const color_element_text = this._render.createElement('div');
      color_element_text.className = 'pp-overview-card-description-wrap-row-color-text';
      color_element_text.innerHTML = 'color: ';

      const color_element_pick = this._render.createElement('div');
      color_element_pick.className = 'pp-overview-card-description-wrap-row-color-pick';
      this._render.setStyle(color_element_pick, 'background', data.color);

      color_element.appendChild(color_element_text);
      color_element.appendChild(color_element_pick);


      const stock_available = this._render.createElement('div');
      stock_available.className = 'pp-overview-card-description-wrap-row-stock';

      const stock_available_text = this._render.createElement('div');
      stock_available_text.className = 'pp-overview-card-description-wrap-row-stock-text';
      stock_available_text.innerHTML = 'In Stock : ';

      const stock_available_number = this._render.createElement('div');
      stock_available_number.className = 'pp-overview-card-description-wrap-row-stock-available';
      const available_number = sessionStorage.getItem(data._id);
      stock_available_number.innerHTML = available_number;

      stock_available.appendChild(stock_available_text);
      stock_available.appendChild(stock_available_number);


      stock_row.appendChild(color_element);
      stock_row.appendChild(stock_available);

      const description_content = this._render.createElement('div');
      description_content.className = 'pp-overview-card-description-wrap-content';
      description_content.innerHTML = data.description;

      desciption_wrapper.appendChild(stock_row);
      desciption_wrapper.appendChild(description_content);

      button_wrapper.parentElement.insertBefore(desciption_wrapper, button_wrapper);
      this._render.setStyle(e.target, 'width', '350px');
      this._render.setStyle(e.target, 'box-shadow', '0 2px 50px #abbaab');


    } else {
      this._render.setStyle(e.target, 'width', '320px');
      this._render.setStyle(e.target, 'box-shadow', 'none');
      e.target.removeChild(check_details_element[0]);
    }
  }

  /**
   * purpose: Emit tha add to cart event to the parent element product page and update the session variable for that id inorder to update stock number.
   * @param e: event
   * @param data: product details item
   */

  addToCart(e, data) {
    e.stopPropagation();
    let current_count = Number(sessionStorage.getItem(data._id));
    if (current_count > 0) {
      current_count -= 1;
      const parent_element = e.target.parentElement.parentElement;
      const available_number = parent_element.getElementsByClassName('pp-overview-card-description-wrap-row-stock-available');
      if (available_number.length !== 0) {
        available_number[0].innerHTML = current_count;
      }
      sessionStorage.setItem(data._id, current_count.toString());
      this.add_to_cart.emit(data);
      e.target.innerHTML = 'Added!';
      setTimeout(() => {
        e.target.innerHTML = 'Add To Cart';
      }, 1000);
    } else {
      e.target.innerHTML = 'No Stock';
      this._render.setStyle(
        e.target,
        'animation',
        'shake 0.82s cubic-bezier(.36,.07,.19,.97) both'
      );
      this._render.setStyle(
        e.target,
        'transform',
        'translate3d(0, 0, 0)'
      );
      this._render.setStyle(
        e.target,
        'backface-visibility',
        'hidden'
      );
      this._render.setStyle(e.target, 'perspective', '1000px');
      this._render.setStyle(e.target, 'background', 'red');
      setTimeout(() => {
        this._render.setStyle(e.target, 'animation', 'none');
        this._render.setStyle(e.target, 'transform', 'none');
        this._render.setStyle(
          e.target,
          'backface-visibility',
          'none'
        );
        e.target.innerHTML = 'Add to cart';
        this._render.setStyle(e.target, 'perspective', 'none');
        this._render.setStyle(
          e.target,
          'background',
          '#abbaab'
        );
      }, 820);
    }

  }

  /**
   * purpose: Interstitial view
   * @param e: event
   * @param data: product details item
   */

  generateQuickView(e, data) {
    const current_count = sessionStorage.getItem(data._id);
    e.stopPropagation();
    this.interstitial_data = data;
    this.interstitial_data.stock.remaining = current_count;
    if (typeof this.quickView !== 'undefined') {
      if (this.quickView.nativeElement.style.display === 'flex') {
        this._render.setStyle(this.quickView.nativeElement, 'display', 'none');
      } else {
        this._render.setStyle(this.quickView.nativeElement, 'display', 'flex');
        this.closeInterstitial.nativeElement.addEventListener('click', event => {
          this._render.setStyle(this.quickView.nativeElement, 'display', 'none');
        });

      }
    }


  }

}
